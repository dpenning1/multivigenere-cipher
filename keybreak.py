def primes(n):    
	if n <= 1:
		print('No primes.')        
		return False    
	n = int(n)       
	p = list(range(1,n+1,2))
	q = len(p)
	p[0] = 2
	for i in range(3,int(n**.5)+1,2):
		if p[(i-1)//2]:            
			p[(i*i-1)//2:q:i] = [0]*((q-(i*i+1)//2)//i+1)           
	return [x for x in p if x]


p = primes(10)
x = 0
first = True
while True:
	s = ''
	stop = True
	for a in xrange(0,len(p)):
		b = x%p[a]
		s += ' ' + str(b)
		if b != 0:
			stop = False
	print s
	
	x += 1
	if stop and not first:
		break
	first = False