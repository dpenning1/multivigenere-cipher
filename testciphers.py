import strongvigenere
import sys
import time
import numpy as np
import matplotlib.pyplot as plt
from Crypto.Cipher import AES


f = open('bible.txt','r')
print "generating plaintext"
bible = ''
for a in f.readlines():
	bible += a
f.close()
f = open('encrypt.txt','w')
for b in xrange(10,11):
	bibletmp = bible
	print "building cipher object"
	encrypt = strongvigenere.strongVigenere(bibletmp,'This is a Test. Carry On')
	start = time.clock()
	plaintext = encrypt.generate_PlainText_Hex()
	elapsed = (time.clock() - start)
	print "generating plaintext hex took " + str(elapsed)
	plain_set = []
	a = 0
	while a < len(plaintext):
		plain_set.append(int(plaintext[a] + plaintext[a+1],16))
		a += 2
	start = time.clock()
	ciphertext = encrypt.generate_CipherText_Hex()
	elapsed = (time.clock() - start)
	print "generating ciphertext hex took " + str(elapsed)
	cipher_set = []
	a = 0
	while a < len(plaintext):
		cipher_set.append(int(ciphertext[a] + ciphertext[a+1],16))
		f.write(ciphertext[a] + ciphertext[a+1])
		if a%100 == 0:
			f.write('\n')
		a += 2
	print "generating frequency tables "
	plaincounts = [0]*256
	for a in plain_set:
		plaincounts[a] += 1
	ciphercounts= [0]*256
	for a in cipher_set:
		ciphercounts[a] += 1
	plt.plot(plaincounts,color='blue',lw=1.5,ls='steps')
	plt.plot(ciphercounts,color='black',lw=1.5,ls='steps')
	plt.show()
f.close()