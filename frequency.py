#Code Written By David Pennington 2/4/2013

import sys
import numpy as np
import matplotlib.pyplot as plt
import random
import hashlib as hl
import strongvigenere as SV

def primes(n):    
	if n <= 1:
		print('No primes.')        
		return False    
	n = int(n)       
	p = list(range(1,n+1,2))
	q = len(p)
	p[0] = 2
	for i in range(3,int(n**.5)+1,2):
		if p[(i-1)//2]:            
			p[(i*i-1)//2:q:i] = [0]*((q-(i*i+1)//2)//i+1)           
	return [x for x in p if x]

def build_Text(source,length):
	#Build a String from a source with all correct characters in source
	if source != 'random':
		s = ''
		f = open(source,'r')
		lines = f.readlines()
		f.close()
		for line in lines:
			#remove new lines and make lowercase
			line = line.lower().replace('\n','')
			tmp_line = ''
			for a in line:
				if 97 <= ord(a) < 123:
					tmp_line += a
			s += tmp_line
		if length == -1:
			return s
		else:
			return s[0:length]
	s = ''
	i = 0
	while i < length:
		s += chr(random.randint(97,122))
		i += 1
	return s

def build_Frequency_List(t):
	#builds a frequency list from a text
	l = [0]*256
	for c in t: l[ord(c)] += 1
	for x in xrange(0,256):
		l[x] = (float(l[x])/len(t))

	return l

def create_Ciphertext(pt,k):
	ct = ''
	for i in xrange(0,len(pt)): ct += chr((((ord(pt[i])-97)+(ord(k[i%len(k)])-97))%26)+97)
	return ct

def generate_random_key(length):
	s = ""
	while len(s)<length: s+= chr(random.randint(97,122))
	return s

def slow_SafeVig(pt,n):
	crypt = SV.strongVigenere(pt,generate_random_key(n),"strong")
	return crypt.generate_CipherText_Hex()

def fast_SafeVig(pt,n):
	crypt = SV.strongVigenere(pt,generate_random_key(n),"medium")
	ct = crypt.generate_CipherText_Hex()
	nct = ''
	for a in xrange(0,len(ct),2):
		nct += chr(int(ct[a] + ct[a+1],16))
	return nct

def main(fi):
	# the main files that brings all of the methods together
	print "generating Plaintext"
	plaintext  = build_Text(fi,-1)
	svnum = 10
	pr = primes(100)
	key_length = 0
	for i in xrange(0,svnum):
		key_length += pr[i]
	print "generating fast SafeVigText"
	fastsafevigtext = fast_SafeVig(plaintext,svnum)
	print "generating frequency lists"
	freq_list_plain = build_Frequency_List(plaintext)
	fast_freq_list_safeVig = build_Frequency_List(fastsafevigtext)
	print "plotting"
	#from here its just code to print graphs
	plt.title('Multi-Vigenere Cipher vs. Plaintext')
	plt.xlabel('byte\'s corresponding value(0-255)')
	plt.ylabel('percentage of byte usage')

	plt.text(0, .115, "Plaintext", fontdict=None, withdash=False,color='blue',size='large')
	plt.text(0, .11, "Ciphertext", fontdict=None, withdash=False,color='green',size='large')

	plt.plot(freq_list_plain,color='blue',lw=1.5,ls='steps')
	plt.plot(fast_freq_list_safeVig,color='green',lw=1.5,ls='steps')
	plt.show()


if __name__ == "__main__":
	#main part of the program
	main('bible.txt')