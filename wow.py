import matplotlib.pyplot as plt

def build_Text(source,length):
	#Build a String from a source with all correct characters in source
	if source != 'random':
		s = ''
		f = open(source,'r')
		lines = f.readlines()
		f.close()
		for line in lines:
			#remove new lines and make lowercase
			line = line.lower().replace('\n','')
			tmp_line = ''
			for a in line:
				if 97 <= ord(a) < 123:
					tmp_line += a
			s += tmp_line
		if length == -1:
			return s
		else:
			return s[0:length]
	s = ''
	i = 0
	while i < length:
		s += chr(random.randint(97,122))
		i += 1
	return s

key = "ciphers"
pt = build_Text('bible.txt',-1)
ct = ""
for a in xrange(0,len(pt)):
	ct += chr((ord(key[a%len(key)])-97+ord(pt[a])-97)%26+97)
ptvals = []
ctvals = []
for a in xrange(0,26):	
	ptvals.append(0)
	ctvals.append(0)
for a in xrange(0,len(pt)):
	ptvals[ord(pt[a])-97] += 1
	ctvals[ord(ct[a])-97] += 1
for a in xrange(0,26):
	ptvals[a] = float(ptvals[a])/float(len(pt))
	ctvals[a] = float(ctvals[a])/float(len(pt))

plt.title('Vigenere Cipher Key=\'CIPHERS\' of The Bible\nSupposed Key length 1')
plt.xlabel('character\'s corresponding value(0(A)-25(Z))')
plt.ylabel('percentage of letter usage')
plt.text(15, .13, "plaintext", fontdict=None, withdash=False,color='blue',size='large')
plt.text(15, .12, "ciphertext", fontdict=None, withdash=False,color='red',size='large')
plt.plot(ptvals,color='blue',lw=4,ls='steps')
plt.plot(ctvals,color='red',lw=4,ls='steps')
plt.show()