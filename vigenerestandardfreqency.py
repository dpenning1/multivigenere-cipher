import sys
import matplotlib.pyplot as plt
import random

def build_Text(source,length):
	#Build a String from a source with all correct characters in source
	if source != 'random':
		s = ''
		f = open(source,'r')
		lines = f.readlines()
		f.close()
		for line in lines:
			#remove new lines and make lowercase
			line = line.lower().replace('\n','')
			tmp_line = ''
			for a in line:
				if 97 <= ord(a) < 123:
					tmp_line += a
			s += tmp_line
		if length == -1:
			return s
		else:
			return s[0:length]
	s = ''
	i = 0
	while i < length:
		s += chr(random.randint(97,122))
		i += 1
	return s

def create_Ciphertext(pt,k):
	ct = ''
	for i in xrange(0,len(pt)): ct += chr((((ord(pt[i])-97)+(ord(k[i%len(k)])-97))%26)+97)
	return ct

def generate_random_key(length):
	s = ""
	while len(s)<length: s+= chr(random.randint(97,122))
	return s


keylength = 7
key = 'ciphers'

plaintext = build_Text('bible.txt',1*10**6)
ciphertext = create_Ciphertext(plaintext,key)
offsetlist = []
for a in xrange(keylength):
	l = []
	for a in xrange(0,256):
		l.append(0)
	offsetlist.append(l)
for a in xrange(1000000):
	offsetlist[a%keylength][ord(ciphertext[a])] += 1

final = []
for a in offsetlist:
	l = []
	for b in a:
		if b != 0:
			l.append(float(b)/(1000000.0/7))
	final.append(l)

offset = []
for a in key:
	offset.append((ord(a)-ord('a')+4)%26)
print offset


plt.title('Shift Cipher Key=\'Cipher\' of The Bible\n Supposed Key Length 7')
plt.xlabel('character\'s corresponding value(0(A)-25(Z))')
plt.ylabel('percentage of letter usage')

plt.text(0, .13, "offset 0", fontdict=None, withdash=False,color='red',size='large')
plt.text(0, .125, "offset 1", fontdict=None, withdash=False,color='orange',size='large')
plt.text(0, .12, "offset 2", fontdict=None, withdash=False,color='yellow',size='large')
plt.text(0, .115, "offset 3", fontdict=None, withdash=False,color='green',size='large')
plt.text(0, .11, "offset 4", fontdict=None, withdash=False,color='blue',size='large')
plt.text(0, .105, "offset 5", fontdict=None, withdash=False,color='purple',size='large')
plt.text(0, .10, "offset 6", fontdict=None, withdash=False,color='black',size='large')

plt.plot(final[0],color='red',lw=4,ls='steps')
plt.plot(final[1],color='orange',lw=4,ls='steps')
plt.plot(final[2],color='yellow',lw=4,ls='steps')
plt.plot(final[3],color='green',lw=4,ls='steps')
plt.plot(final[4],color='blue',lw=4,ls='steps')
plt.plot(final[5],color='purple',lw=4,ls='steps')
plt.plot(final[6],color='black',lw=4,ls='steps')
plt.show()
