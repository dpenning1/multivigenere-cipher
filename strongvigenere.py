#imports 
import hashlib as hl
import random

#class for use in other programs
class strongVigenere:

	def __init__(self,new_plain_text,new_key,strength):
		self._key = None
		self._hash = None
		self._hex_hash = None
		self._bin_hash = None
		if strength == "medium":
			self._hash_alg  = "sha1"
		else:
			self._hash_alg  = "sha256"
		self.change_PlainText(new_plain_text)
		self.__change_Key(new_key)
		self.__generate_Hex_Hash()

	def change_PlainText(self,npt):
		#changes the plaintext
		self._plain_text = npt

	def __change_Key(self,new_key):
		self._key = new_key
		if self._hash_alg == "sha1":
			self._hash = hl.sha1()
		else:
			self._hash = hl.sha256()
		self._hash.update(self._key)
		self.__generate_Hex_Hash()

	def __generate_Bin_Hash(self):
		#generates a binary hash value
		self._bin_hash = bin(int(self._hex_hash,16))[2:]
		while len(self._bin_hash) < len(self._hex_hash)*4:
			self._bin_hash = '0'+ self._bin_hash

	def __generate_Hex_Hash(self):
		self._hex_hash = self._hash.hexdigest()
		self.__generate_Bin_Hash()

	def print_Hash_Type(self):print (self._hash_alg)

	def print_Hex_Hash(self):print (self._hex_hash)

	def print_Hex_Hash(self):print (self._bin_hash)

	def __primes(self,n):
		if n <= 1:
			print('No primes.')        
			return False    
		n = int(n)       
		p = list(range(1,n+1,2))
		q = len(p)
		p[0] = 2
		for i in range(3,int(n**.5)+1,2):
			if p[(i-1)//2]:            
				p[(i*i-1)//2:q:i] = [0]*((q-(i*i+1)//2)//i+1)           
		return [x for x in p if x]

	def generate_PlainText_Hex(self):
		s = ''
		for a in self._plain_text:
			tmp_hex = hex(ord(a))[2:]
			while len(tmp_hex) < 2:
				tmp_hex = '0' + tmp_hex
			s += tmp_hex
		return s

	def generate_CipherText_Hex(self):
		random.seed(self._hex_hash)
		hash_blocks = []
		a = 0
		while a <len(self._hex_hash):
			s = str(self._hex_hash[a]) + str(self._hex_hash[a+1])
			hash_blocks.append(int(s,16))
			a += 2
		p = self.__primes(1620)
		keys = []
		for a in xrange(0,len(self._bin_hash)):
			if self._bin_hash[a] == '1':
				keys.append(p[a])
		for a in xrange(0,len(keys)):
			length = keys[a]
			key_insert = []
			for b in xrange(0,length):
				key_insert.append(random.randint(0,255))
			keys[a] = key_insert
		iterators = [0]*len(keys)
		ct = ''
		percent = 0
		for a in xrange(0,len(self._plain_text)):
			if a*10/len(self._plain_text) > percent:
				percent = a*10/len(self._plain_text)
				print percent*10
			shift = 0
			for b in xrange(0,len(iterators)):
				iterators[b] += 1
				if iterators[b] == len(keys[b]):
					iterators[b] = 0
				shift += keys[b][iterators[b]]
			shift %= 256
			tmp_hex = hex((ord(self._plain_text[a])+shift)%255)[2:]
			while len(tmp_hex) < 2:
				tmp_hex = '0' + tmp_hex
			ct += tmp_hex
		return ct

if __name__ == "__main__":
	x = strongVigenere('Check this out, dont get all mad now','This is a Test. Carry On',"medium")
	print x.print_Hex_Hash()
	print x.print_Hash_Type()
	print x.generate_PlainText_Hex()
	print x.generate_CipherText_Hex()
